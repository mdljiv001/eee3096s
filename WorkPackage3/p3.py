# Import libraries
import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os

# some global variables that need to change as we run the program
end_of_game = False  # set if the user wins or ends the game

# DEFINE THE PINS USED HERE
LED_value = [11, 13, 15]
LED_accuracy = 32
btn_submit = 16
btn_increase = 18
buzzer = 33
eeprom = ES2EEPROMUtils.ES2EEPROM()


# Print the game banner
def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print("| \ | |               | |                / ____| |          / _|/ _| |")
    print("|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print("| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print("| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print("|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")


# Print the game menu
def menu():
    global end_of_game
    option = input("Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()
    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)
    elif option == "P":
        os.system('clear')
        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")
        value = generate_number()
        while not end_of_game:
            pass
    elif option == "Q":
        print("Come back soon!")
        exit()
    else:
        print("Invalid option. Please select a valid one!")


def display_scores(count, raw_data):
    # print the scores to the screen in the expected format
    print("There are {} scores. Here are the top 3!".format(count))
    # print out the scores in the required format
    for k in range(0,3):
        print((i+1),"- {} took {} guesses".format(raw_data[k][0]+raw_data[k][1]+raw_data[k][2],str(raw_data[k][0])))
    pass


# Setup Pins
def setup():
    # Setup board mode
    GPIO.setmode(GPIO.BOARD)
    # Setup regular GPIO
    GPIO.setup(LED_value[0],GPIO.OUT)
    GPIO.setup(LED_value[1],GPIO.OUT)
    GPIO.setup(LED_value[2],GPIO.OUT)
    GPIO.setup(LED_accuracy,GPIO.OUT)
    GPIO.setup(btn_increase,GPIO.IN,pull_up_down=GPIO.PUD_up)
    GPIO.setup(btn_submit,GPIO.IN,pull_up_down=GPIO.PUD_up)
    GPIO.setup(buzzer,GPIO.OUT)


    # Setup PWM channels
    global LED_pwm
    LED_pwm=GPIO.PWM(LED_accuracy,1000)
    global buzz_pwm
    buzz_pwm=GPIO.PWM(buzzer,1000)
    # Setup debouncing and callbacks
    GPIO.add_event_detect(btn_submit,GPIO.FALLING,callback=btn_guess_pressed,bouncetime=400)
    GPIO.add_event_detect(btn_increase,GPIO.FALLING,callback=btn_increase_pressed,bouncetime=400)

    pass


# Load high scores
def fetch_scores():
    # get however many scores there are
    score_count = eeprom.read_byte(0)
    # Get the scores
    scores=[]
    for k in range(1, score_count+1):
        scores.append(eeprom.read_block(i,4))
    # convert the codes back to ascii
    for i in range(0,score_count):
        for d in range(0,3):
            scores[i][d]=chr(scores[i][d])
    # return back the results
    return score_count, scores


# Save high scores
def save_scores(num_guesses,names):
    # fetch scores
    score_count,scores= fetch_scores()
    # include new score
    scores.append([names[0],names[1],names[2],num_guesses])
    # sort
    scores.sort(key=lambda x: x[3])

    # update total amount of scores
    score_count=score_count+1
    # write new scores
    data_to_write=[]
    for n in scores:
        for p in range(0,3):
            data_to_write.append(ord(n[p])

        data_to_write.append(n[3])    
    eeprom.write_block(1,data_to_write)
    eeprom.write_byte(0,score_count)
    pass


# Generate guess number
def generate_number():
    return random.randint(0, pow(2, 3)-1)


# Increase button pressed
def btn_increase_pressed(channel):
    # Increase the value shown on the LEDs
    global LED_val
    LED_val+=1
    if LED_val>7:
        LED_val=0

    GPIO.output(LED_value[0],(LED_val & 0b001)!=0)   
    GPIO.output(LED_value[1],(LED_val & 0b010)!=0)   
    GPIO.output(LED_value[2],(LED_val & 0b100)!=0)   
    # You can choose to have a global variable store the user's current guess, 
    # or just pull the value off the LEDs when a user makes a guess
    pass


# Guess button
def btn_guess_pressed(channel):
    # If they've pressed and held the button, clear up the GPIO and take them back to the menu screen
    start=time.time()
    while GPIO.input(btn_submit)==GPIO.LOW:
        time.sleep(0.01)
    length=time.time()-start
    if length>1.5:
        clear()
        reset()
        end_of_game=True
        menu()
    LED_pwm.stop()
    buzz_pwm.stop()        

    # Compare the actual value with the user value displayed on the LEDs
    global guess
    global num_guess
    global play

    guesses+=1
    if (guess!=value):
        accuracy_leds()
    # Change the PWM LED
    # if it's close enough, adjust the buzzer
        if (abs(guess-value)<4):
            trigger_buzzer() 
    
    # if it's an exact guess:
    elif (guess==value):
        clear()
    # - Disable LEDs and Buzzer
     
    # - tell the user and prompt them for a name
    name=input("You won!! Please enter name ")+"   "
    name=name[:3]
    # - fetch all the scores
    # - add the new score
    # - sort the scores
    # - Store the scores back to the EEPROM, being sure to update the score count
    save_scores(name,guesses)
    reset()
    end_of_game=True
    menu()

    pass


# LED Brightness
def accuracy_leds():
    # Set the brightness of the LED based on how close the guess is to the answer
    # - The % brightness should be directly proportional to the % "closeness"
    # - For example if the answer is 6 and a user guesses 4, the brightness should be at 4/6*100 = 66%
    # - If they guessed 7, the brightness would be at ((8-7)/(8-6)*100 = 50%
    volt=1.0
    if guess<value:
        volt=abs((float(guess)/value)*100)
    elif guess>value:
        volt=abs(((8-float(guess))/(8-value))*100)    
    LED_pwm.start(volt)
    LED_pwm.CHangeDutyCycle(volt)

    pass

# Sound Buzzer
def trigger_buzzer():
    # The buzzer operates differently from the LED
    # While we want the brightness of the LED to change(duty cycle), we want the frequency of the buzzer to change
    # The buzzer duty cycle should be left at 50%
    # If the user is off by an absolute value of 3, the buzzer should sound once every second
    # If the user is off by an absolute value of 2, the buzzer should sound twice every second
    # If the user is off by an absolute value of 1, the buzzer should sound 4 times a second

    volt=50.0
    freq=1.0
    if (abs(guess-value)==3):
        freq=1.0
    elif (abs(guess-value)==2):
        freq=2.0
    elif (abs(guess-value)==1):
        freq=4.0
    buzz_pwm.start(volt)
    buzz_pwm.ChangeFrequency(freq)
    pass

def clear():
    GPIO.output(LED_value[0],0)
    GPIO.output(LED_value[1],0)
    GPIO.output(LED_value[2],0)
    LED_pwm.stop()
    buzz_pwm.stop()

def reset():
    global guesses
    guesses=0
    global guess
    guess=0
    global play
    play=False
    global value
    value=0

    



if __name__ == "__main__":
    try:
        # Call setup function
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
